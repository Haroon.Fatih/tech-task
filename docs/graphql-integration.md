# Integration with graphqL endpoints

Instruction on integrating with a graphql endpoint with auto generated types based on BE schema.

## Setup with Codegen

### Installation
```
npm install graphql
npm install -D typescript
npm install -D @graphql-codegen/cli
```

### Creating a config file
Once finished create a file called `codegen.ts`. Below is an example that is running against [PDA](https://gitlab.com/r2617/evaluate/property-details-api). I had to run PDA (our BE) project locally instead of using QA/PROD.

```
import type { CodegenConfig } from '@graphql-codegen/cli';

// run your BE locally to generate a typed schema for FE
const config: CodegenConfig = {
    schema: 'http://localhost:8080/graphql',
    documents: ['app/**/*.tsx', 'app/**/*.ts'],
    ignoreNoDocuments: true,
    generates: {
        './gql/': {
            preset: 'client',
            plugins: [],
        },
    },
};
export default config;

```

### Running the code generator
Then run the following - More information on codegen docs and options - https://the-guild.dev/graphql/codegen/docs/config-reference/codegen-config

```
npx graphql-code-generator --config ./path/to/codegen.ts
```

Add a script to your `package.json` to generate it manually whenever your schema needs updating.

```
"codegen": "graphql-codegen -r dotenv/config",
```

### The output
The commands above create a `gql` folder with the following:

```
- gql
    - fragment-masking.ts
    - gql.ts  // graphql query function
    - graphql.ts // auto generated types based on graphql schema
    - index.ts
```

## How to use it


Install a graphql client, in this case `graphql-request`
```
npm add graphql-request
```

### Creating a graphql client

Whichever folder you have your server code in, create a file called `grahpql-client.ts` with the following code.

```
import { GraphQLClient } from 'graphql-request';

// process.env.{whatever your endpoint url is}
export const graphqlClient = new GraphQLClient(process.env.PROPERTY_DETAILS_API_URL as string);
```

In Next 13 we are directly referencing `process.env` directly as opposed to `getConfig()`, this is also possible in next 12 - [more info on configuration here](configuration.md)

### Creating a qraphql query

In same folder where `graphql-client.ts` is create a query file.

For example, below I am calling sold property data from PDA so I created a file called `sold-property-client.ts` which exports a function called `GetSoldPricesData` which our auto generated `graphql` query function from gql file.

```
import { graphql } from '../gql';

export const GetSoldPricesData = graphql(/* GraphQL */ `
    query soldProperty($propertyId: ID, $transactionId: ID!, $country: String!) {
        soldProperty(id: $propertyId, transactionId: $transactionId, country: $country) {
            property {
                id
            ...}
        ...}
    ...}
```

### Fetching data in Server Components

Inside of `Page.tsx` or `Layout.tsx` we can call our query with the expected parameters. For `GetSoldPricesData` it would like so.

``` 
const soldPricesData = await graphqlClient.request(GetSoldPricesData, { 
    country: 'england',
    propertyId: '123',
    transactionId: '234',
});
```

By the time you're calling this `soldPricesData` is fully typed based on your schema without the need to create a custome types file for the BE.

### Updating the Auto type schema

To do so, run the `package.json` script we added running against a local instance of your BE and it will auto generate the latest schema.