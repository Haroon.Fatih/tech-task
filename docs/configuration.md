# Env variables configuration

If you opt to use the `/app` directory in nextjs 13 you can no longer use `getConfig()` from `next/config` to reference runtime configuration.
work is planned in the nextjs roadmap but nothing concerete yet - [roadmap link](https://beta.nextjs.org/docs/app-directory-roadmap#planned-features)

## What to use with /app
Use `process.env` on the server and for client prefix your variables with `NEXT_PUBLIC`
[Docs on client side variables](https://nextjs.org/docs/basic-features/environment-variables)

## what to use with /pages
If you are using `/pages` you __can__ still use runtime config, however it is considered legacy.
[Docs on runtime connfiguration](https://nextjs.org/docs/api-reference/next.config.js/runtime-configuration)
