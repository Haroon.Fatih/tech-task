"use client"

import React from 'react'
import { properties } from "../pages/api/data";
import  { useState } from 'react';
// import  Button, { ButtonColours, ButtonSizes } from 'kanso-react/lib/ssr/design-system/Button';
import Button from 'kanso-react/lib/ssr/design-system/Button';
import Image from "next/image";

properties.sort((a,b) => {
    return b.pricing.price - a.pricing.price;
})


    export const Compwithalldata = () => {

        const styleone = {
            color: "blue",
            float: "left",
            border: "5px solid blue",
            borderRadius: "10px",
        }

        const styletwo = {
            color: "green",
            float: "right",

        }

        const stylethree = {
            color: "red",
            float: "right",
        }

        const stylefour = {
            color: "deeppurple",
        }


        const overallstyle = {
            backgroundColor:  "lightgreen",
            borderRadius: "20px",
            margin: "10px",
            border: "5px solid black",
            padding: "25px",
        }

        // const [isHovered, setIsHovered] = useState(false);
            
        

        console.log(properties)

        return (
          <>
              <Button
                  colour={Button.PRIMARY_CORE}
              >
                  Max Rooms
              </Button>
              <br />
              <Button
                  colour={Button.PRIMARY_CORE}
              >
                  Min Rooms
              </Button>
              <br />
              <Button
                  colour={Button.PRIMARY_CORE}
              >
                  Max Price
              </Button>
              <br />
              <Button
                  colour={Button.PRIMARY_CORE}
              >
                  Min Price
              </Button>
              <br />

          {
              properties.map((property)=> {
                  return (
                      <>
    {/*                  <div*/}
    {/*  onMouseEnter={() => setIsHovered(true)}*/}
    {/*  onMouseLeave={() => setIsHovered(false)}*/}
    {/*>*/}

                      <div style={overallstyle}>


                      <div style = {styleone} key={property.id}>
                          Property image: {property.uuid}
                          Property image https://rightmove.co.uk/dir/crop/10:9-16:9/{property.uuid}

                      </div>


                      <div style = {styletwo} key={property.id}>
                          Property price: {property.pricing.price}
                      </div>

                      <br></br>

                      <div style = {stylethree} key={property.id}>
                          Number of bedrooms: {property.listingInfo.bedrooms}

                      </div>
                      <br></br>

                      <div style = {stylefour} key={property.id}>
                          <b>Property address: {property.address.streetName}</b>
                          <br></br>
                          <br></br>
                          Property description: {property.listingInfo.description}
                      </div>

                      </div>
                      {/*</div>*/}
                      </>

                  )
              })
          }
      </>
        )
      }