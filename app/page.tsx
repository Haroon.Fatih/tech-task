import Image from 'next/image'
import styles from './page.module.css'
import {Compwithalldata} from './propertydata'

export default function Home() {
  return (
    <section className={styles.main}>
     <Compwithalldata />
    </section>
  )
}
