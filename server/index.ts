import next from 'next';
import getConfig from 'next/config';
import express from 'express';
import { appSetup } from 'rightmove-platform/core';
import { getLogger } from 'rightmove-platform/utils';
import Server from './core/Server';
import { contextMiddleware } from 'request-storage';

(async () => {
    const dev = process.env.NODE_ENV !== 'production';
    const hostname = process.env.APPLICANTION_HOSTNAME || '0.0.0.0';
    const port = process.env.APPLICANTION_PORT ? parseInt(process.env.APPLICANTION_PORT) : 3000;
    const app = next({ dev, hostname, port });

    await app.prepare();
    const application = express();
    const config = getConfig().serverRuntimeConfig;

    application.use(contextMiddleware);

    appSetup.initAppConfig({
        map: new Map(Object.entries(config)),
        vaultCredentialsKey: config.vaultKey
    });

    appSetup.initLogging();

    application.all('*', (req, res) => {
        app.getRequestHandler()(req, res);
    });

    const applicationServer = new Server(config.applicationName, port, application, getLogger(), !dev);
    await applicationServer.start();
})();