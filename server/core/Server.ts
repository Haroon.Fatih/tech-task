import * as https from 'https';
import * as http from 'http';
import { appConfig, getLogger } from 'rightmove-platform/utils';
import type { RequestListener } from 'http';

class Server {
    private readonly _application: RequestListener;
    private readonly _isHTTPS: boolean;
    private readonly _logger: any;
    private readonly _name: string;
    private readonly _port: number;

    constructor(name: string, port: number, application: RequestListener, logger: ReturnType<typeof getLogger>, isHTTPS = false) {
        this._application = application;
        this._isHTTPS = isHTTPS;
        this._logger = logger;
        this._name = name;
        this._port = port;
    }

    public async start(): Promise<void> {
        const server = await this._createServer();
        return new Promise((resolve, reject) => {
            server.listen(this._port, '0.0.0.0', (err?: Error) => {
                if (err) {
                    return reject(new Error(`${this._name} could not be started: ${err.message}`));
                }

                const startUpMessage = `${this._name} server running on :${this._port}`;

                if (this._isHTTPS) {
                    startUpMessage.concat(' with HTTPS');
                }

                this._logger.info(startUpMessage);
                return resolve();
            })
        });
    }

    private async _getTLSOptions(): Promise<https.ServerOptions> {
        const key = await appConfig.getVaultValue('tls_key', true);
        const cert = await appConfig.getVaultValue('tls_cert', true);
        return { key, cert };
    }

    private async _createServer(): Promise<http.Server | https.Server> {
        if (this._isHTTPS) {
            const options = await this._getTLSOptions();
            return https.createServer(options, this._application);
        }

        return http.createServer(this._application);
    }
}

export default Server;
