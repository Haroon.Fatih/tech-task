/** @type {import('next').NextConfig} */
const nextConfig = {
  serverRuntimeConfig: {
    applicationName: process.env.APPLICATION_NAME,
    vaultKey: process.env.VAULT_KEY,
    seoURL: process.env.SEO_URL
  },
  experimental: {
    appDir: true,
  },
}

module.exports = nextConfig
